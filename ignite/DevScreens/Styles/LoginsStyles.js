import { StyleSheet } from 'react-native'
import { Metrics, ApplicationStyles } from '../DevTheme/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    paddingTop: Metrics.section,
    backgroundColor: '#3e243f',
    flex: 1
  },
  Input: {
    height: 40,
    backgroundColor: 'rgba(255,255,255,0.2)',
    marginBottom: 15,
    color: 'black',
    paddingHorizontal: 10,
    borderRadius: 40,
    fontSize: 17
  },
  logoContainer: {
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingTop: 50
  },
  logo: {
    width: 150,
    height: 180,
    alignItems: 'center',
    justifyContent: 'center',
    resizeMode: 'stretch',
    marginBottom: 40,
    paddingTop: 40
  },
  buttomContainer: {
    backgroundColor: '#2980b9',
    paddingVertical: 15,
    marginBottom: 10,
    borderRadius: 30
  },
  buttomText: {
    textAlign: 'center',
    color: '#ffffff',
    fontWeight: '700'
  }
})
