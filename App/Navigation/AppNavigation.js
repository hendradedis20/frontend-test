import { StackNavigator } from 'react-navigation'
import LaunchScreen from '../Containers/LaunchScreen'
import PresentationScreen from '../../ignite/DevScreens/PresentationScreen'
import TabOptions from '../../ignite/DevScreens/TabOptions'
import LoginScreen from '../../ignite/DevScreens/LoginScreen'

import styles from './Styles/NavigationStyles'

// Manifest of possible screens
const PrimaryNav = StackNavigator({
  LaunchScreen: { screen: LaunchScreen },
  LoginScreen: { screen: LoginScreen },
  firstscreen: {screen: PresentationScreen},
  TabOptions: { screen: TabOptions }
}, {
  // Default config for all screens
  headerMode: 'none',
  initialRouteName: 'LaunchScreen',
  navigationOptions: {
    headerStyle: styles.header
  }
})

export default PrimaryNav
