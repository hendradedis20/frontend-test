# Yors Mobile Frontend Test

[![js-standard-style](https://img.shields.io/badge/code%20style-standard-brightgreen.svg?style=flat)](http://standardjs.com/)



* Take home test project React Native App Utilizing [Ignite](https://github.com/infinitered/ignite)


###  Introduction
This test is simulation test to represent about how and what we do day by day at Yors, if you pass this test then we can asume you are ready to join us.


### :arrow_up: Preparation


**Step 1:** Please fork the code and create your own repository base on this test repo. Do not commit on my branch or repo


**Step 2:** Follow the instruction test.


**Step 3:** Commit your answer code and email me where you store your code, make sure you give proper access to me. You can use any SCM you like


**Step 4:** If you pass this test you can move to the next step of interview.


### :arrow_forward: Instruction


**Note:** This test was create with Ignite boilerplate you can pull origin repository or use this repo.


**Task.1:** Please create new navigation on the bottom of the page and the navigation should appear on every page. Navigation can slide up and show subcategory please look **Pic.2**


| Pic.1        | Pic.2           | Pic.3  |
| ------------- |:-------------:| ------|
|  <img src="https://s3-ap-southeast-1.amazonaws.com/yors-test/1.jpg" align="left" height="450" width="250" >  | <img src="https://s3-ap-southeast-1.amazonaws.com/yors-test/2.jpg" align="left" height="450" width="250" > | <img src="https://s3-ap-southeast-1.amazonaws.com/yors-test/3.jpg" align="left" height="450" width="250" > |

**Task.2:** Please create new feature QR code scanner, you can use any QR code scanner library and attach the function to the button on **Task.1**.

**Task.3:** Get and pass the information to the new page. see **Pic.3**

**Task.4** Make sure it can run on both side android and IOS, if you don't have iphone or android you can use cloud service or any 3rd party apps on the market.


### :closed_lock_with_key: Objective

**Obj 1:** Comply all instruction

**Obj 2:** Deliver the answer with good and clean code

**Obj 3:** Make sure every class, parameter, and function name understandable



### :arrow_up: Score Criteria

**-** If you achieve all objective and deliver ontime you will get score 80


**-** If you give unit testing for what you do, you will get extra score 20


**-** If you add 1 aditional function to this project based on your creativity and if it useful you will get extra score 20

### sorry i cant solve all issue , still have any Debug. because i can work issue in friday. i have problme in project because i can't don't really understand for redux. and i cant not exploit the time due to much my actifity, so sorry sir.

### : My Actifity
**-** first day i read and try understand for flow app, because i really understand for redux

**-** day two i do it for add tabNavigation i have problem in how way separate TabNavigation and StackNavigator in tobe swipeUp tabNavigator. i solve but swipe up cant put together for stacknavigation

**-** next i try for divide be Component TabOption and File Presentasion. emm sucsess but so icant solve how merge Tabnavigtion with stacknavigator.

**-** last day so i i cant solve swipe and i try for add Scanner Code with library moaazisidat react-native-qrcode-scanner but i have problame in permision device in gradle and info.plist . but I've activated the camera successfully but here I am not. and last i try add fiture login and accses example endpoint rest HTTP API apiary Token accses but cant work navigate in branch "add-Login"  end then finaly time is over

**-** so the next day i cant do much , which i can do add scanner Qr code , and the stackNavigator bug inside TabNavigator i cant solve. i am sorry sir, and the state of the library scannerQr can not be parsed to another screen, i cant show in alert, and bug in android gradle i cant put solusion how gradle right, because there are several package in not support gradle 3 , in sdk 26 so crash in android. but in ios run. so you can try in iOS. i dont know so I've done as well but result no as good as i thought. so iam very sorry sir. problame is same , i cant understand Redux saga, i need time more time. so i hope to do better again//.
